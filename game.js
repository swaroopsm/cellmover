/**
 * Game Board
 */
function Board(rows, cols) {
  this.$el = document.getElementById('board');
  this.cells = [];

  for(var i=0; i<rows; i++) {
    this.cells[i] = [];
    for(var j=0; j<cols; j++) {
      this.cells[i][j] = new Cell(i, j, this);
    }
  }
}

Board.prototype.getCellAt = function(x, y) {
  return typeof this.cells[x] !== 'undefined' &&
         typeof this.cells[x][y] !== 'undefined' &&
         this.cells[x][y];
};

Board.prototype.render = function() {
  var nodes = [];

  this.cells.forEach(function(cells) {
    cells.forEach(function(cell) {
      var el = cell.render();
      this.$el.appendChild(el);
    }.bind(this));
  }.bind(this));

  this.$el.addEventListener('click', function(e) {

    var el = e.target,
        x = el.dataset.x,
        y = el.dataset.y,
        cell = this.getCellAt(+x, +y),
        freeCell;

    if(el.tagName !== 'LI') { return; }
    freeCell = cell.canMove();
    if(!freeCell) { return; }

    this.swapCellValues(cell, freeCell);

    // Check whether game is finished
    if(this.hasGameFinished()) { this.finishGame(); }
  }.bind(this));
};

Board.prototype.hasGameFinished = function() {
 return this.isSorted(); 
};

Board.prototype.finishGame = function() {
  alert('Yay! You Win!');
};

Board.prototype.isSorted = function() {
  var cellList = [],
      isSorted = false;

  // Hack
  // Screws performance
  var flattenCells = function() {
    for(var i=0, rows=this.cells.length; i<rows; i++) {
      var cells = this.cells[i];

      for(j=0, cols=cells.length; j<cols; j++) {
        cellList.push(cells[j]);
      }
    }

    return cellList;
  }.bind(this);
  flattenCells();

  for(var k=0, klength=cellList.length; k<klength; k++) {
    var nextCell = cellList[k+1];

    if(nextCell) {
      if(cellList[k].value > nextCell.value) {
        isSorted = false
        return isSorted;
      }
      else {
        isSorted = true;
      }
    }
  }

  return isSorted;
};

Board.prototype.swapCellValues = function(src, target) {
  var srcValue = src.value;
      targetValue = target.value;

  src.updateValue(targetValue);
  target.updateValue(srcValue);
};

/**
 * Cell
 */
function Cell(x, y, board) {
  this.x = x;
  this.y = y;
  this.board = board;
  this.value = null;
}

Cell.prototype.render = function() {
  var el;

  this.$el = this.$el ? this.$el : document.createElement('li');
  el = this.$el;

  el.setAttribute('data-x', this.x);
  el.setAttribute('data-y', this.y);
  el.setAttribute('data-x', this.x);
  el.setAttribute('data-value', this.value);

  if(this.value === null) {
    el.innerHTML = '&nbsp;';
  }
  else {
    el.innerHTML = this.value;
  }

  return el;
};

Cell.prototype.canMove = function() {
  var neighbors = this.getNeighbors(),
      cell;

  for(var i=0, length=neighbors.length; i<length; i++) {
    cell = neighbors[i];
    if(cell.isFree()) { return cell; }
  }

  return false;
};

Cell.prototype.getNeighbors = function() {
  var neighbors;

  neighbors = [
    this.board.getCellAt(this.x-1, this.y),
    this.board.getCellAt(this.x+1, this.y),
    this.board.getCellAt(this.x, this.y-1),
    this.board.getCellAt(this.x, this.y+1),
  ];

  return neighbors.filter(function(n) {
    return n !== false;
  });
},

Cell.prototype.isFree = function() {
  return this.value === null; 
};

Cell.prototype.updateValue = function(newVal) {
  this.value = newVal;
  this.render();
};


// Game Start
window.game = (function() {

  return {
    init: function() {
      var board = new Board(3, 3);

      // Hardcode the values for now
      board.getCellAt(0, 0).value = 1;
      board.getCellAt(0, 1).value = 3;
      board.getCellAt(0, 2).value = 4;

      board.getCellAt(1, 0).value = 5;
      board.getCellAt(1, 1).value = 6;
      board.getCellAt(1, 2).value = 7;

      board.getCellAt(2, 0).value = 8;
      board.getCellAt(2, 1).value = 2;

      // Initial Render
      board.render()
    }
  };

}());
